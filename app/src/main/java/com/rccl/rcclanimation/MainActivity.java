package com.rccl.rcclanimation;

import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private GLSurfaceView surfaceView;
    private NativeRenderer renderer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String waterModelPath = Utils.copyAssetToCache(this, "water.obj");
        String waterTexturePath = Utils.copyAssetFolderToCache(this, "water");

        String cruiseModelPath = Utils.copyAssetToCache(this, "cruise.obj");
        String cruiseTexturePath = Utils.copyAssetToCache(this, "cruise.png");

        surfaceView = new GLSurfaceView(this);
        surfaceView.setEGLContextClientVersion(2);
        renderer = new NativeRenderer();
        surfaceView.setRenderer(renderer);
        renderer.setWaterPaths(waterModelPath, waterTexturePath);
        renderer.setCruisePaths(cruiseModelPath, cruiseTexturePath);
        surfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getPointerCount() == 1 &&
                        event.getAction() == MotionEvent.ACTION_DOWN) {
                    renderer.onClick(event.getX(), event.getY());
                }
                return true;
            }
        });
        setContentView(surfaceView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        surfaceView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        surfaceView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        renderer.onDestroy();
    }
}
