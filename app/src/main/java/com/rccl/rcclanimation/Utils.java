package com.rccl.rcclanimation;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since 12/02/2017.
 */
public class Utils {
    private Utils() {
        // Nothing
    }

    @NonNull
    private static String getCacheDirectory(@NonNull Context context) {
        File cacheDir = context.getExternalCacheDir();
        if (cacheDir == null) {
            cacheDir = context.getCacheDir();
        }
        return cacheDir.getAbsolutePath() + File.separator;
    }

    @NonNull
    private static String copyAssetToCache(@NonNull Context context,
                                           @NonNull String destinyPath,
                                           @NonNull String assetName) {
        AssetManager assetManager = context.getAssets();
        if (!destinyPath.endsWith(File.separator)) {
            destinyPath += File.separator;
        }
        String toPath = destinyPath + assetName;
        try {
            InputStream in = assetManager.open(assetName);
            File destiny = new File(toPath);
            if (!destiny.exists()) {
                destiny.createNewFile();
                OutputStream out = new FileOutputStream(toPath);
                copyFile(in, out);
                out.flush();
                out.close();
            }
            in.close();
        } catch (IOException e) {
            Log.e(Utils.class.getSimpleName(), e.getMessage(), e);
        }
        return toPath;
    }

    @NonNull
    public static String copyAssetToCache(@NonNull Context context, @NonNull String assetName) {
        String cacheDirectory = getCacheDirectory(context);
        return copyAssetToCache(context, cacheDirectory, assetName);
    }

    @NonNull
    public static String copyAssetFolderToCache(@NonNull Context context, @NonNull String folder) {
        AssetManager assetManager = context.getAssets();
        String outputDirectory = getCacheDirectory(context) + folder;
        try {
            String[] files = assetManager.list(folder);
            File outputFile = new File(outputDirectory);
            if (!outputFile.exists()) {
                outputFile.mkdirs();
            }
            for (String file : files) {
                copyAssetToCache(context, folder + File.separator + file);
            }
        } catch (IOException e) {
            Log.e(Utils.class.getSimpleName(), e.getMessage(), e);
        }
        return outputDirectory;
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }
}
