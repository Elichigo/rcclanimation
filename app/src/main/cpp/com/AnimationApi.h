//
// Created by Fernando on 11/02/2017.
//

#ifndef RCCLANIMATION_ANIMATIONAPI_H
#define RCCLANIMATION_ANIMATIONAPI_H

#include "../PlatformProvider.h"
#include <malloc.h>
#include "utils/Logger.h"
#include "../PlatformProvider.h"
#include "render/World.h"
#include "../libs/glm/glm.hpp"
#include "../libs/glm/gtc/matrix_transform.hpp"
#include "utils/OpenGLUtils.h"
#include "utils/ConstantValues.h"
#include "utils/OBBCollision.h"

using namespace glm;

class AnimationApi {
private:
    const char *V_POSITION = "vPosition";
    const char *V_TEX_COORD = "vUv";
    const char *TEXTURE_SAMPLER = "texture";
    const char *MODEL_MATRIX = "model";
    const char *VIEW_MATRIX = "view";
    const char *PROJECTION_MATRIX = "projection";

    GLuint programId;
    Logger *logger = PlatformProvider::getLogger();
    World *world = World::getInstance();
    int screenWidth;
    int screenHeight;

    void releaseShaders(GLuint vertexShader, GLuint fragmentShader) const;

    bool createProgram(GLuint vertexShader, GLuint fragmentShader);

    bool checkProgramStatus() const;

public:
    bool initOpenGL();

    void setViewPort(int width, int height);

    bool loadWater(const char *objPath, const char *texturePath);

    bool loadShip(const char *objPath, const char *texturePath);

    void draw();

    int click(float x, float y);

    ~AnimationApi();
};


#endif //RCCLANIMATION_ANIMATIONAPI_H
