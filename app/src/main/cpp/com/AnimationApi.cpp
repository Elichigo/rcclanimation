//
// Created by Fernando on 11/02/2017.
//

#include "AnimationApi.h"

bool AnimationApi::initOpenGL() {
    GLuint vertexShader = OpenGLUtils::loadShader(GL_VERTEX_SHADER);
    GLuint fragmentShader = OpenGLUtils::loadShader(GL_FRAGMENT_SHADER);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_ALPHA);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_CULL_FACE);
    return createProgram(vertexShader, fragmentShader);
}

void AnimationApi::releaseShaders(GLuint vertexShader,
                                  GLuint fragmentShader) const {
    glDetachShader(programId, vertexShader);
    glDetachShader(programId, fragmentShader);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

bool AnimationApi::createProgram(GLuint vertexShader,
                                 GLuint fragmentShader) {
    programId = glCreateProgram();
    if (programId == 0) {
        logger->log(Logger::ERROR, "Program has not been created");
        return false;
    } else {
        glAttachShader(programId, vertexShader);
        glAttachShader(programId, fragmentShader);

        glBindAttribLocation(programId, ConstantValues::vPositionId, V_POSITION);
        glBindAttribLocation(programId, ConstantValues::vTexCoordId, V_TEX_COORD);

        glLinkProgram(programId);

        ConstantValues::modelMatrixId = glGetUniformLocation(programId, MODEL_MATRIX);
        ConstantValues::viewMatrixId = glGetUniformLocation(programId, VIEW_MATRIX);
        ConstantValues::projectionMatrixId = glGetUniformLocation(programId, PROJECTION_MATRIX);
        ConstantValues::textureSamplerId = glGetUniformLocation(programId, TEXTURE_SAMPLER);

        releaseShaders(vertexShader, fragmentShader);
        return checkProgramStatus();
    }
}

bool AnimationApi::checkProgramStatus() const {
    GLint linked;
    glGetProgramiv(programId, GL_LINK_STATUS, &linked);
    if (!linked) {
        GLint infoLength = 0;
        glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &infoLength);
        if (infoLength > 1) {
            char *infoLog = (char *) malloc(sizeof(char) * infoLength);
            glGetProgramInfoLog(programId, infoLength, NULL, infoLog);
            logger->log(Logger::ERROR, infoLog);
            free(infoLog);
        }
        return false;
    }
    return true;
}

void AnimationApi::setViewPort(int width,
                               int height) {
    screenWidth = width;
    screenHeight = height;
    glViewport(0, 0, screenWidth, screenHeight);
    ConstantValues::projectionMatrix = perspective(
            radians(45.0f),
            (float) screenWidth / (float) screenHeight,
            1.0f,
            100.0f
    );
}

bool AnimationApi::loadWater(const char *objPath,
                             const char *texturePath) {
    Water *water = new Water();
    bool result = water->load(objPath, texturePath);
    TextureAnimation *animation = new TextureAnimation();
    animation->setDuration(5000);
    water->setAnimation(animation);
    world->setWater(water);
    return result;
}

bool AnimationApi::loadShip(const char *objPath,
                            const char *texturePath) {
    Ship *ship = new Ship();
    bool result = ship->load(objPath, texturePath);
    world->setShip(ship);
    return result;
}

void AnimationApi::draw() {
    glUseProgram(programId);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.1f, 0.2f, 0.4f, 1.0f);
    world->draw();
}

AnimationApi::~AnimationApi() {
    glFlush();
    glFinish();
    glDeleteProgram(programId);
    world->~World();
}

int AnimationApi::click(float x, float y) {
    vec3 rayOrigin, rayDirection;
    OBBCollision::screenPositionToWorldRay(
            x,
            y,
            screenWidth,
            screenHeight,
            Camera::getInstance()->getViewMatrix(),
            ConstantValues::projectionMatrix,
            rayOrigin,
            rayDirection);
    float intersectionDistance;
    bool result = OBBCollision::testRayObbIntersection(
            rayOrigin,
            rayDirection,
            vec3(-3.0f, -3.0f, 0.0f),
            vec3(3.0f, 3.0f, 0.0f),
            world->getShip()->getModelMatrix(),
            intersectionDistance);
    return result ? 1 : 0;
}
