//
// Created by Jesús Fernando Sierra Pastrana on 15/02/17.
//

#ifndef RCCLANIMATION_ANIMATION_H
#define RCCLANIMATION_ANIMATION_H

#include <chrono>
#include "Interpolator.h"

using namespace std::chrono;

class Animation {
    friend class Animable;

public:
    typedef enum {
        ONE_SHOT,
        REVERSE,
        INFINITE,
        INFINITE_REVERSE
    } RepetitionMode;

protected:
    Logger *logger = PlatformProvider::getLogger();
    RepetitionMode repetitionMode = ONE_SHOT;
    Interpolator *interpolator = new Interpolator();
    time_point<system_clock> startTime;
    long duration;
    bool running = false;
    bool reverse = false;

    void start() {
        if (!running) {
            startTime = high_resolution_clock::now();
            running = true;
        }
    }

    void calculate() {
        auto currentTime = high_resolution_clock::now();
        if (running) {
            long delta = duration_cast<milliseconds>(currentTime - startTime).count();
            double scale = (double) delta / (double) (duration);
            scale = scale > 1.0 ? 1.0 : scale;
            double animationStep = interpolator->getInterpolation(scale);
            if (reverse) {
                animationStep = 1 - animationStep;
            }
            animateStep(animationStep);

            if (scale == 1.0) {
                handleAnimationEnd();
            }
        }
    }

    void handleAnimationEnd() {
        switch (repetitionMode) {
            case ONE_SHOT:
                stop();
                break;
            case REVERSE:
                if (reverse) {
                    stop();
                } else {
                    startTime = high_resolution_clock::now();
                    reverse = true;
                }
                break;
            case INFINITE:
                startTime = high_resolution_clock::now();
                break;
            case INFINITE_REVERSE:
                startTime = high_resolution_clock::now();
                reverse = !reverse;
                break;
        }
    }

    virtual void animateStep(double step) {}

public:

    void setDuration(long milliseconds) { duration = milliseconds; }

    void setRepetitionMode(RepetitionMode mode) { repetitionMode = mode; }

    void transform() {
        if (running) {
            calculate();
        }
    }

    bool isRunning() const {
        return running;
    }

    void stop() {
        running = false;
    }
};

#endif //RCCLANIMATION_ANIMATION_H
