//
// Created by Jesús Fernando Sierra Pastrana on 15/02/17.
//

#ifndef RCCLANIMATION_TEXTUREMANAGER_H
#define RCCLANIMATION_TEXTUREMANAGER_H

#include <map>
#include <vector>
#include "../../PlatformProvider.h"
#include "OpenGLUtils.h"
#include "../../libs/soil/SOIL.h"

using namespace std;

class TextureManager {
private:
    Logger *logger = PlatformProvider::getLogger();
    map<int, vector<GLuint>> texturesMap;
public:
    void loadTextures(int objectId, vector<const char *> textures);

    vector<GLuint> getTexturesList(int objectId);

    void clear(int objectId);

    ~TextureManager();
};


#endif //RCCLANIMATION_TEXTUREMANAGER_H
