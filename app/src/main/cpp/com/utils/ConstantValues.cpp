//
// Created by Fernando on 12/02/2017.
//

#include "ConstantValues.h"

GLint ConstantValues::modelMatrixId = 0;
GLint ConstantValues::viewMatrixId = 0;
GLint ConstantValues::projectionMatrixId = 0;
GLint ConstantValues::textureSamplerId = 0;
mat4 ConstantValues::projectionMatrix = mat4(1.0f);