//
// Created by Fernando on 12/02/2017.
//

#ifndef RCCLANIMATION_IDSHOLDER_H
#define RCCLANIMATION_IDSHOLDER_H

#include "../../PlatformProvider.h"
#include "../../libs/glm/glm.hpp"
#include "../../libs/glm/gtc/matrix_transform.hpp"

using namespace glm;

class ConstantValues {
private:
    ConstantValues() {}

public:
    static const GLuint vPositionId = 0;
    static const GLuint vTexCoordId = 1;
    static GLint modelMatrixId;
    static GLint viewMatrixId;
    static GLint projectionMatrixId;
    static GLint textureSamplerId;
    static mat4 projectionMatrix;
};

#endif //RCCLANIMATION_IDSHOLDER_H
