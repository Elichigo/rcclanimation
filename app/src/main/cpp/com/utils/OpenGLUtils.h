//
// Created by Fernando on 12/02/2017.
//

#ifndef RCCLANIMATION_OPENGLUTILS_H
#define RCCLANIMATION_OPENGLUTILS_H

#include <malloc.h>
#include <vector>
#include <stdio.h>
#include <string>
#include "Logger.h"
#include "../../PlatformProvider.h"
#include "../../libs/glm/glm.hpp"
#include "ConstantValues.h"
#include "../render/Camera.h"

using namespace std;
using namespace glm;

static Logger *logger = PlatformProvider::getLogger();
static const char *vertexShaderSrc =
        "uniform mat4 model;\n"
                "uniform mat4 view;\n"
                "uniform mat4 projection;\n"
                "attribute vec2 vUv;\n"
                "attribute vec3 vPosition;\n"
                "varying vec2 UV;\n"
                "void main()\n"
                "{\n"
                "    UV = vUv;\n"
                "    gl_Position = projection * view * model * vec4(vPosition, 1.0);\n"
                "}";
static const char *fragmentShaderSrc =
        "precision mediump float;\n"
                "uniform sampler2D texture;\n"
                "varying vec2 UV;\n"
                "void main()\n"
                "{\n"
                "    gl_FragColor = texture2D(texture, UV);\n"
                "}";

class OpenGLUtils {
private:
    OpenGLUtils() {}

public:

    static GLuint loadShader(GLenum type) {
        GLuint shader;
        shader = glCreateShader(type);
        if (shader == 0) {
            return 0;
        }
        const char *shaderSrc = type == GL_VERTEX_SHADER ? vertexShaderSrc : fragmentShaderSrc;
        glShaderSource(shader, 1, &shaderSrc, NULL);
        glCompileShader(shader);
        checkShaderStatus(shader);
        return shader;
    }

    static void checkShaderStatus(GLuint shader) {
        GLint compiled;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint infoLength = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLength);
            if (infoLength > 1) {
                char *infoLog = (char *) malloc(sizeof(char *) * infoLength);
                glGetShaderInfoLog(shader, infoLength, NULL, infoLog);
                logger->log(Logger::ERROR, infoLog);
                free(infoLog);
            }
            glDeleteShader(shader);
        }
    }

    static bool loadObj(const char *objPath,
                        vector<vec3> &outVertexes,
                        vector<vec3> &outNormals,
                        vector<vec2> &outUvs) {
        vector<unsigned int> vertexIndexes, uvIndexes, normalIndexes;
        vector<vec3> tempVertexes;
        vector<vec2> tempUvs;
        vector<vec3> tempNormals;

        FILE *file = fopen(objPath, "r");
        if (file == NULL) {
            logger->log(Logger::ERROR, "Invalid OBJ file");
            return false;
        }

        char lineHeader[1024];
        int res = fscanf(file, "%s", lineHeader);;
        while (res != EOF) {
            if (strcmp(lineHeader, "v") == 0) { // Vertex line
                vec3 vertex;
                fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
                tempVertexes.push_back(vertex);
            } else if (strcmp(lineHeader, "vt") == 0) { //Texel line
                vec2 uv;
                fscanf(file, "%f %f\n", &uv.x, &uv.y);
                tempUvs.push_back(uv);
            } else if (strcmp(lineHeader, "vn") == 0) { //Normal line
                vec3 normal;
                fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
                tempNormals.push_back(normal);
            } else if (strcmp(lineHeader, "f") == 0) { //Face line
                string vertex1, vertex2, vertex3;
                unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
                int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0],
                                     &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1],
                                     &normalIndex[1], &vertexIndex[2], &uvIndex[2],
                                     &normalIndex[2]);
                if (matches != 9) {
                    logger->log(Logger::ERROR, "Error while parsing faces");
                    return false;
                }
                vertexIndexes.push_back(vertexIndex[0]);
                vertexIndexes.push_back(vertexIndex[1]);
                vertexIndexes.push_back(vertexIndex[2]);
                uvIndexes.push_back(uvIndex[0]);
                uvIndexes.push_back(uvIndex[1]);
                uvIndexes.push_back(uvIndex[2]);
                normalIndexes.push_back(normalIndex[0]);
                normalIndexes.push_back(normalIndex[1]);
                normalIndexes.push_back(normalIndex[2]);
            }
            res = fscanf(file, "%s", lineHeader);
        }

        for (unsigned int i = 0; i < vertexIndexes.size(); i++) {
            unsigned int vertexIndex = vertexIndexes[i];
            vec3 vertex = tempVertexes[vertexIndex - 1];
            outVertexes.push_back(vertex);
        }

        for (unsigned int i = 0; i < normalIndexes.size(); i++) {
            unsigned int normalIndex = normalIndexes[i];
            vec3 normal = tempNormals[normalIndex - 1];
            outNormals.push_back(normal);
        }

        for (unsigned int i = 0; i < uvIndexes.size(); i++) {
            unsigned int uvIndex = uvIndexes[i];
            vec2 uv = tempUvs[uvIndex - 1];
            outUvs.push_back(uv);
        }
        return true;
    }

    template<typename T>
    static GLuint createBuffer(const vector<T> &input) {
        GLuint bufferId;
        glGenBuffers(1, &bufferId);
        glBindBuffer(GL_ARRAY_BUFFER, bufferId);
        glBufferData(GL_ARRAY_BUFFER, input.size() * sizeof(T), &input[0], GL_STATIC_DRAW);
        return bufferId;
    }

    static void setMatrices(mat4 modelMatrix) {
        glUniformMatrix4fv(ConstantValues::projectionMatrixId, 1, GL_FALSE,
                           &ConstantValues::projectionMatrix[0][0]);
        glUniformMatrix4fv(ConstantValues::viewMatrixId, 1, GL_FALSE,
                           &Camera::getInstance()->getViewMatrix()[0][0]);
        glUniformMatrix4fv(ConstantValues::modelMatrixId, 1, GL_FALSE,
                           &modelMatrix[0][0]);
    }
};

#endif //RCCLANIMATION_OPENGLUTILS_H
