//
// Created by Fernando on 11/02/2017.
//

#ifndef RCCLANIMATION_LOGGER_H
#define RCCLANIMATION_LOGGER_H


class Logger {
protected:
    const char *TAG_LOG = "com.rccl.native";
public:
    typedef enum {
        DEBUG,
        ERROR,
        INFO,
        VERBOSE
    } LogLevel;

    virtual void log(LogLevel level, const char *message) {}
};


#endif //RCCLANIMATION_LOGGER_H
