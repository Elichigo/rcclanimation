//
// Created by Jesús Fernando Sierra Pastrana on 15/02/17.
//

#include "TextureManager.h"

void TextureManager::loadTextures(int objectId, vector<const char *> textures) {
    vector<GLuint> texturesList;
    for (const char *texture : textures) {
        GLuint textureId = SOIL_load_OGL_texture(texture,
                                                 SOIL_LOAD_AUTO,
                                                 SOIL_CREATE_NEW_ID,
                                                 SOIL_FLAG_MIPMAPS | SOIL_FLAG_NTSC_SAFE_RGB |
                                                 SOIL_FLAG_COMPRESS_TO_DXT);
        if (0 == textureId){
            logger->log(Logger::ERROR, SOIL_last_result());
        }
        if (textureId > 0) {
            texturesList.push_back(textureId);
        }
    }
    texturesMap[objectId] = texturesList;
}

vector<GLuint> TextureManager::getTexturesList(int objectId) {
    return texturesMap[objectId];
}

void TextureManager::clear(int objectId) {
    for (GLuint id : texturesMap[objectId]) {
        glDeleteTextures(1, &id);
    }
}

TextureManager::~TextureManager() {
    texturesMap.clear();
}
