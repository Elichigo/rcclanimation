//
// Created by Fernando on 11/02/2017.
//

#include "World.h"

World *World::instance = nullptr;

World *World::getInstance() {
    if (instance == nullptr) {
        instance = new World();
    }
    return instance;
}

void World::draw(Renderable *renderable) {
    if (renderable != nullptr) {
        OpenGLUtils::setMatrices(renderable->getModelMatrix());
        renderable->draw();
    }
}

void World::draw() {
    if (water != nullptr) {
        water->startAnimation();
    }
    draw(water);
    draw(land);
    draw(ship);
}

void World::setWater(Water *water) {
    World::water = water;

    TextureAnimation *animation = new TextureAnimation();
    animation->setRepetitionMode(Animation::INFINITE_REVERSE);
    animation->setDuration(5000);

    World::water->setAnimation(animation);
}

void World::setLand(Land *land) {
    World::land = land;
}

void World::setShip(Ship *ship) {
    World::ship = ship;
}

Ship *World::getShip() const {
    return ship;
}

Water *World::getWater() const {
    return water;
}
