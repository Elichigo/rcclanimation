//
// Created by Jesús Fernando Sierra Pastrana on 14/02/17.
//

#ifndef RCCLANIMATION_CAMERA_H
#define RCCLANIMATION_CAMERA_H

#include "../../libs/glm/glm.hpp"
#include "../../libs/glm/gtc/matrix_transform.hpp"

using namespace glm;

class Camera {
private:
    static Camera *instance;
    mat4 matrix = lookAt(
            vec3(0.0f, 3.0f, 0.0f), //Camera position
            vec3(0.0f, 0.0f, 0.0f), //Looking at
            vec3(0.0f, 0.0f, 1.0f)  //Head up
    );

    Camera() {}

public:
    static Camera *getInstance();

    const mat4 &getViewMatrix() const;

    void setViewMatrix(const mat4 &matrix);
};


#endif //RCCLANIMATION_CAMERA_H
