//
// Created by Jesús Fernando Sierra Pastrana on 15/02/17.
//

#include "MultiTextureRenderable.h"

void MultiTextureRenderable::loadTexture(const char *texturePath) {
    vector<const char *> textures;
    struct dirent *entry;
    DIR *dir = opendir(texturePath);
    if (dir == NULL) {
        return;
    }
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 &&
            strcmp(entry->d_name, "..") != 0) {
            char *str = new char[512];
            strcpy(str, texturePath);
            strcat(str, "/");
            strcat(str, entry->d_name);
            textures.push_back(str);
        }
    }
    closedir(dir);
    textureManager->loadTextures(id, textures);
    texturesVector = textureManager->getTexturesList(id);
}

GLuint MultiTextureRenderable::getTextureToDisplay() {
    TextureAnimation *textureAnimation = (TextureAnimation *) animation;
    return texturesVector.empty() ?
           0 :
           texturesVector[textureAnimation == nullptr || !textureAnimation->isRunning() ?
                          0 :
                          textureAnimation->getCurrentTexturePosition()];
}

void MultiTextureRenderable::setAnimation(TextureAnimation *anim) {
    Animable::setAnimation(anim);
    anim->setTexturesTotal(texturesVector.size());
}
