//
// Created by Fernando on 11/02/2017.
//

#include "Renderable.h"

int Renderable::instancesCounter = 0;

Renderable::Renderable() {
    id = ++instancesCounter;
    textureManager = new TextureManager();
}

void Renderable::loadTexture(const char *texturePath) {
    vector<const char *> textures;
    textures.push_back(texturePath);
    textureManager->loadTextures(id, textures);
    texturesVector = textureManager->getTexturesList(id);
}

bool Renderable::load(const char *objPath,
                      const char *texturePath) {
    bool result = OpenGLUtils::loadObj(objPath, vertexes, normals, uvs);
    vertexesBufferId = OpenGLUtils::createBuffer(vertexes);
    normalsBufferId = OpenGLUtils::createBuffer(normals);
    uvsBufferId = OpenGLUtils::createBuffer(uvs);
    loadTexture(texturePath);
    return result;
}

void Renderable::activateAndBindUvs(GLuint vUvId) const {
    glEnableVertexAttribArray(vUvId);
    glBindBuffer(GL_ARRAY_BUFFER, uvsBufferId);
    glVertexAttribPointer(vUvId, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);
}

void Renderable::activateAndBindVertexes(GLuint vPositionId) const {
    glEnableVertexAttribArray(vPositionId);
    glBindBuffer(GL_ARRAY_BUFFER, vertexesBufferId);
    glVertexAttribPointer(vPositionId, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);
}

void Renderable::activateAndBindTexture(GLuint textureId) const {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glUniform1i(ConstantValues::textureSamplerId, 0);
}

void Renderable::disableAttributes(GLuint vPositionId,
                                   GLuint vUvId) const {
    glDisableVertexAttribArray(vPositionId);
    glDisableVertexAttribArray(vUvId);
}

void Renderable::draw() {
    if (animation != nullptr){
        animation->transform();
    }
    GLuint vPositionId = ConstantValues::vPositionId;
    GLuint vUvId = ConstantValues::vTexCoordId;
    activateAndBindTexture(getTextureToDisplay());
    activateAndBindVertexes(vPositionId);
    activateAndBindUvs(vUvId);
    //TODO Use normals

    glDrawArrays(GL_TRIANGLES, 0, (GLsizei) vertexes.size());

    disableAttributes(vPositionId, vUvId);
}

const mat4 &Renderable::getModelMatrix() const {
    return modelMatrix;
}

Renderable::~Renderable() {
    glDeleteBuffers(1, &vertexesBufferId);
    glDeleteBuffers(1, &uvsBufferId);
    glDeleteBuffers(1, &normalsBufferId);
    textureManager->clear(id);
}

GLuint Renderable::getTextureToDisplay() {
    return texturesVector.empty() ? 0 : texturesVector[0];
}
