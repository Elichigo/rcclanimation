//
// Created by Jesús Fernando Sierra Pastrana on 14/02/17.
//

#include "Camera.h"

Camera *Camera::instance = nullptr;

const mat4 &Camera::getViewMatrix() const {
    return matrix;
}

void Camera::setViewMatrix(const mat4 &matrix) {
    Camera::matrix = matrix;
}

Camera *Camera::getInstance() {
    if (instance == nullptr) {
        instance = new Camera();
    }
    return instance;
}
