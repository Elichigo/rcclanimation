//
// Created by Fernando on 11/02/2017.
//

#include "Ship.h"

Ship::Ship() {
    modelMatrix = scale(modelMatrix, vec3(0.3f, 0.3f, 0.3f));
    modelMatrix = translate(modelMatrix, vec3(0.0f, 0.1f, 0.0f));
}
