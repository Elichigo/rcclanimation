//
// Created by Fernando on 11/02/2017.
//

#ifndef RCCLANIMATION_WORLD_H
#define RCCLANIMATION_WORLD_H

#include "Water.h"
#include "Land.h"
#include "Ship.h"
#include "../utils/ConstantValues.h"

class World {
private:
    static World *instance;
    Camera *camera = nullptr;
    Water *water = nullptr;
    Land *land = nullptr;
    Ship *ship = nullptr;

    World() {
        camera = Camera::getInstance();
    }

    void draw(Renderable *renderable);

public:
    static World *getInstance();

    void setWater(Water *water);

    Water *getWater() const;

    void setLand(Land *land);

    void setShip(Ship *ship);

    Ship *getShip() const;

    void draw();

    ~World() {
        camera->~Camera();
        if (water != nullptr) {
            water->~Water();
        }
        if (ship != nullptr) {
            ship->~Ship();
        }
        if (land != nullptr) {
            land->~Land();
        }
    }
};


#endif //RCCLANIMATION_WORLD_H
