//
// Created by Jesús Fernando Sierra Pastrana on 15/02/17.
//

#ifndef RCCLANIMATION_MULTITEXTURERENDERABLE_H
#define RCCLANIMATION_MULTITEXTURERENDERABLE_H


#include "Renderable.h"
#include <dirent.h>
#include <string.h>
#include <vector>
#include "../animation/TextureAnimation.h"

class MultiTextureRenderable : public Renderable {

protected:
public:
    virtual void setAnimation(TextureAnimation *anim);

protected:

    virtual void loadTexture(const char *texturePath);

    virtual GLuint getTextureToDisplay();
};


#endif //RCCLANIMATION_MULTITEXTURERENDERABLE_H
