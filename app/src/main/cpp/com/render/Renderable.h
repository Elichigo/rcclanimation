//
// Created by Fernando on 11/02/2017.
//

#ifndef RCCLANIMATION_RENDEROBJECT_H
#define RCCLANIMATION_RENDEROBJECT_H

#include "../../PlatformProvider.h"
#include <vector>
#include "../../libs/glm/glm.hpp"
#include "../../libs/glm/gtc/matrix_transform.hpp"
#include "../utils/ConstantValues.h"
#include "../../libs/soil/SOIL.h"
#include "../utils/OpenGLUtils.h"
#include "../utils/TextureManager.h"
#include "../animation/Animable.h"

using namespace std;
using namespace glm;

class Renderable : public Animable {
private:
    static int instancesCounter;

protected:
    int id;
    mat4 modelMatrix = mat4(1.0f); //Identity matrix
    vector<vec3> vertexes;
    vector<vec2> uvs;
    vector<vec3> normals;
    GLuint vertexesBufferId;
    GLuint uvsBufferId;
    GLuint normalsBufferId;
    TextureManager *textureManager;
    vector<GLuint> texturesVector;

    void activateAndBindTexture(GLuint textureId) const;

    void activateAndBindVertexes(GLuint vPositionId) const;

    void activateAndBindUvs(GLuint vUvId) const;

    void disableAttributes(GLuint vPositionId, GLuint vUvId) const;

    virtual void loadTexture(const char *texturePath);

    virtual GLuint getTextureToDisplay();

public:

    Renderable();

    const mat4 &getModelMatrix() const;

    bool load(const char *objPath, const char *texturePath);

    void draw();

    ~Renderable();
};


#endif //RCCLANIMATION_RENDEROBJECT_H
