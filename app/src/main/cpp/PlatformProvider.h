//
// Created by Fernando on 11/02/2017.
//

#ifndef RCCLANIMATION_PLATFORMPROVIDER_H
#define RCCLANIMATION_PLATFORMPROVIDER_H

#include "com/utils/Logger.h"

#ifdef __ANDROID__

#include <GLES2/gl2.h>
#include "android/AndroidLogger.h"

class PlatformProvider {
public:
    static Logger *getLogger() {
        return new AndroidLogger();
    }
};

#elif __APPLE__

#include "TargetConditionals.h"
#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE

#include <OpenGLES/ES2/gl.h>
#include "ios/IOSLogger.h"

class PlatformProvider {
public:
    static Logger *getLogger() {
        return new IOSLogger();
    }
};

#endif //IPHONE

#endif //APPLE

#endif //RCCLANIMATION_PLATFORMPROVIDER_H
