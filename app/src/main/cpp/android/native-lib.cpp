#include <jni.h>
#include "../com/AnimationApi.h"

extern "C" {
AnimationApi *animationApi = new AnimationApi();

JNIEXPORT jboolean JNICALL
Java_com_rccl_rcclanimation_NativeRenderer_initOpenGL(
        JNIEnv *env,
        jobject instance) {
    return (jboolean) animationApi->initOpenGL();
}

JNIEXPORT void JNICALL
Java_com_rccl_rcclanimation_NativeRenderer_setViewPort(
        JNIEnv *env,
        jobject instance,
        jint width,
        jint height) {
    animationApi->setViewPort(width, height);
}

JNIEXPORT void JNICALL
Java_com_rccl_rcclanimation_NativeRenderer_draw(JNIEnv *env,
                                                jobject instance) {
    animationApi->draw();
}

JNIEXPORT void JNICALL
Java_com_rccl_rcclanimation_NativeRenderer_releaseResources(
        JNIEnv *env,
        jobject instance) {
    animationApi->~AnimationApi();
}

JNIEXPORT jboolean JNICALL
Java_com_rccl_rcclanimation_NativeRenderer_loadWater(
        JNIEnv *env,
        jobject instance,
        jstring jModelPath,
        jstring jTexturePath) {
    const char *modelPath = env->GetStringUTFChars(jModelPath, 0);
    const char *texturePath = env->GetStringUTFChars(jTexturePath, 0);
    bool result = animationApi->loadWater(modelPath, texturePath);
    env->ReleaseStringUTFChars(jModelPath, modelPath);
    env->ReleaseStringUTFChars(jTexturePath, texturePath);
    return (jboolean) result;
}

JNIEXPORT jboolean JNICALL
Java_com_rccl_rcclanimation_NativeRenderer_loadShip(
        JNIEnv *env,
        jobject instance,
        jstring modelPath_,
        jstring texturePath_) {
    const char *modelPath = env->GetStringUTFChars(modelPath_, 0);
    const char *texturePath = env->GetStringUTFChars(texturePath_, 0);
    bool result = animationApi->loadShip(modelPath, texturePath);
    env->ReleaseStringUTFChars(modelPath_, modelPath);
    env->ReleaseStringUTFChars(texturePath_, texturePath);
    return (jboolean)result;
}

JNIEXPORT jint JNICALL
Java_com_rccl_rcclanimation_NativeRenderer_click(
        JNIEnv *env,
        jobject instance,
        jfloat x,
        jfloat y) {
    return animationApi->click(x, y);
}
}
